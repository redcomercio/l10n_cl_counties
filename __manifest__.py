# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    "name": "Chile Localization Regions, Cities and Counties",
    "version": "1.1",
    "description": """
Chilean Regions, Cities and Counties .

Lista de regiones, ciudades y comunas de Chile.
    """,
    "author": "Cubic ERP, Blanco Martín & Asociados, Odoo Chile Community",
    "website": "http://odoochile.org",
    "category": "Localization/Toponyms",
    "depends": [
            "base_state_ubication",
        ],
    "data": [
        "l10n_cl_counties_data.xml",
        ],
    'init_xml': [
        'query.sql'
        ],
    "active": False,
    "installable": True,
}
